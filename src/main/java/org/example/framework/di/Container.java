package org.example.framework.di;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.framework.di.annotation.Autowired;
import org.example.framework.di.annotation.Component;
import org.example.framework.di.exception.*;
import org.example.framework.di.processor.BeanPostProcessor;
import org.reflections.Reflections;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.*;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Slf4j
public class Container {

    private final Map<String, Class<?>> definitions = new HashMap<>();
    private final Map<String, Object> beans = new HashMap<>();
    private final List<BeanPostProcessor> postProcessors;
    private boolean wired = false;

    public void register(final String pkg) {
        final Reflections reflections = new Reflections(pkg);
        final Set<Class<?>> clazzez = reflections.getTypesAnnotatedWith(Component.class);
        if (clazzez.isEmpty()) {
            return;
        }
        for (Class<?> clazz : clazzez) {
            register(clazz);
        }
    }

    public void register(final Class<?> clazz) {
        register(clazz.getName(), clazz);
    }

    public void register(final Object bean) {
        register(bean.getClass().getName(), bean);
    }

    public synchronized void register(final String name, final Class<?> clazz) {
        assertNotWired();
        assertNameNotRegistered(name);
        definitions.put(name, clazz);
    }
    public synchronized void register(final String name, final Object bean) {
        assertNotWired();
        assertNameNotRegistered(name);
        beans.put(name, bean);
        log.debug("bean created: {}: {}", name, bean.getClass().getName());
    }
  

    public synchronized void wire() {
        wired = true;
        while (definitions.size() != 0) {
            Optional<Object> bean = Optional.empty();
            HashSet<String> names = new HashSet<>(definitions.keySet());
            for (String name : names) {
                log.debug("try created bean: {}", name);
                final Class<?> clazz = definitions.get(name);
                bean = nextGeneration(clazz);

                if (!bean.isPresent()) {
                    continue;
                }
                Object processedBean = bean.get();
                for (BeanPostProcessor postProcessor : postProcessors) {
                    if (postProcessor.canProcessed(bean.get().getClass())) {
                        processedBean = postProcessor.process(processedBean, bean.get().getClass());
                    }
                }
                beans.put(name, processedBean);
                definitions.remove(name, clazz);
                log.debug("bean created: {}: {}", name, clazz.getName());
            }
            bean.orElseThrow(UnmetDependencyException::new);

        }
        autowire();
    }

    private void autowire() {
        List<Object> autowiredBeans = getBeansByAnnotation(Autowired.class);
        if (autowiredBeans.isEmpty()) {
            return;
        }
        for (Object autowiredBean : autowiredBeans) {
            final List<Method> methods = Arrays.stream(autowiredBean.getClass().getDeclaredMethods())
                    .filter(o -> o.isAnnotationPresent(Autowired.class))
                    .collect(Collectors.toList());
            if (methods.isEmpty()) {
                continue;
            }
            for (Method method : methods) {
                final Parameter[] parameters = method.getParameters();
                for (final Parameter parameter : parameters) {
                    if (List.class.isAssignableFrom(parameter.getType())) {
                        final ParameterizedType parameterizedType = (ParameterizedType) parameter.getParameterizedType();
                        final Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                        if (actualTypeArguments.length != 1) {
                            continue;
                        }
                        final List<Object> arguments = getBeansByType(((Class<Object>) actualTypeArguments[0]));
                        try {
                            method.invoke(autowiredBean, arguments);
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            throw new AutowiredException(e);
                        }
                    }
                }
            }
        }
    }

    public synchronized Object getBean(final String name) {
        assertWired();
        return Optional.ofNullable(beans.get(name)).orElseThrow(BeanNotFoundException::new);

    }

    public synchronized <T> T getBean(Class<T> typeClazz) {
        List<T> beans = getBeansByType(typeClazz);
        if (beans.isEmpty()) {
            throw new BeanNotFoundException(typeClazz.getName());
        }
        if (beans.size() > 1) {
                throw new AmbigiousClassException();
            }
        return beans.get(0);
    }

    public synchronized List<Object> getBeansByAnnotation(Class<? extends Annotation> annotationClazz) {
        assertWired();
        return beans.values().stream()
                .filter(o -> o.getClass().isAnnotationPresent(annotationClazz))
                .collect(Collectors.toList());
    }

    public synchronized <T> List<T> getBeansByType(Class<T> typeClazz) {
        assertWired();
        return beans.values().stream()
                .filter(o -> typeClazz.isAssignableFrom(o.getClass()))
                .map(o -> (T) o)
                .collect(Collectors.toList());
    }

    public <T> T getBean(final String name, final Class<T> clazz) {
        return (T) getBean(name);
    }

    private void assertNotWired() {
        if (wired) {
            throw new InvalidContainerStateException();
        }
    }

    private void assertNameNotRegistered(String name) {
        if (definitions.containsKey(name) || beans.containsKey(name)) {
            throw new NameAlreadyRegisteredException();
        }
    }

    private void assertWired() {
        if (!wired) {
            throw new InvalidContainerStateException();
        }
    }

    private Optional<Object> nextGeneration(final Class<?> clazz) {

        final Constructor<?>[] constructors = clazz.getConstructors();
        if (constructors.length != 1) {
            try {
                constructors[0] = clazz.getConstructor();
            } catch (NoSuchMethodException e) {
                throw new AmbigiousConstructorException("bean must have one public constructor or default constructor: " + clazz.getName());
            }
        }
        final Constructor<?> constructor = constructors[0];
        final Parameter[] parameters = constructor.getParameters();
        final List<Object> args = new ArrayList<>(parameters.length);

        for (final Parameter parameter : parameters) {
            Optional<?> arg = instantiated(parameter);
            if (!arg.isPresent()) {
                return Optional.empty();
            }
            args.add(arg.get());
        }
        try {
            return Optional.of(constructor.newInstance(args.toArray()));
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new BeanInstantiationException(e);
        }
    }

    private Optional<?> instantiated(final Parameter parameter) {
        final Class<?> parameterClazz = parameter.getType();
        return beans.values().stream()
                .filter(o -> parameterClazz.isAssignableFrom(o.getClass()))
                .findFirst();
    }
}
