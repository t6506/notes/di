package org.example.framework.di.exception;

public class AmbigiousConstructorException extends RuntimeException {
    public AmbigiousConstructorException() {
    }

    public AmbigiousConstructorException(final String message) {
        super(message);
    }

    public AmbigiousConstructorException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AmbigiousConstructorException(final Throwable cause) {
        super(cause);
    }

    public AmbigiousConstructorException(final String message, final Throwable cause, final boolean enableSuppression,
                                         final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
