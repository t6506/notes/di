package org.example.framework.di.exception;

public class UnmetDependencyException extends RuntimeException {
    public UnmetDependencyException() {
    }

    public UnmetDependencyException(final String message) {
        super(message);
    }

    public UnmetDependencyException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public UnmetDependencyException(final Throwable cause) {
        super(cause);
    }

    public UnmetDependencyException(final String message, final Throwable cause, final boolean enableSuppression,
                                    final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
