package org.example.framework.di.exception;

public class AutowiredException extends RuntimeException {
    public AutowiredException() {
    }

    public AutowiredException(final String message) {
        super(message);
    }

    public AutowiredException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AutowiredException(final Throwable cause) {
        super(cause);
    }

    public AutowiredException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
