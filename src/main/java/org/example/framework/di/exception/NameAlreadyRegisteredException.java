package org.example.framework.di.exception;

public class NameAlreadyRegisteredException extends RuntimeException {
    public NameAlreadyRegisteredException() {
    }

    public NameAlreadyRegisteredException(final String message) {
        super(message);
    }

    public NameAlreadyRegisteredException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public NameAlreadyRegisteredException(final Throwable cause) {
        super(cause);
    }

    public NameAlreadyRegisteredException(final String message, final Throwable cause, final boolean enableSuppression,
                                          final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
