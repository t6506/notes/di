package org.example.framework.di.exception;

public class BeanInstantiationException extends RuntimeException {
    public BeanInstantiationException() {
    }

    public BeanInstantiationException(final String message) {
        super(message);
    }

    public BeanInstantiationException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public BeanInstantiationException(final Throwable cause) {
        super(cause);
    }

    public BeanInstantiationException(final String message, final Throwable cause, final boolean enableSuppression,
                                      final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
