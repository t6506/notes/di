package org.example.framework.di.exception;

public class InvalidContainerStateException extends RuntimeException {
    public InvalidContainerStateException() {
    }

    public InvalidContainerStateException(final String message) {
        super(message);
    }

    public InvalidContainerStateException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public InvalidContainerStateException(final Throwable cause) {
        super(cause);
    }

    public InvalidContainerStateException(final String message, final Throwable cause, final boolean enableSuppression,
                                          final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
